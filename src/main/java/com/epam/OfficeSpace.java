package com.epam;

import java.util.*;

public class OfficeSpace {

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);


    }

    private List<List<Integer>> permutations(List<Integer> res, List<List<Integer>> tails){
        List<List<Integer>> resNEW = new ArrayList<>();
        for(List<Integer> tail: tails){

            for(int n: tail){
                List<List<Integer>> newTails = new ArrayList<List<Integer>>();
                List<Integer> nr = new ArrayList<Integer>();
                nr.addAll(res);
                nr.add(n);
                List<Integer> nt = new ArrayList<Integer>();
                nt.addAll(tail);
                nr.remove(n);
                newTails.add(nr);
                resNEW.addAll(permutations(nr, newTails));
            }
        }
        return resNEW;
    }

    private Office stack(List<Integer> projects, int floorSize){
        Office office = new Office(floorSize);
        for(int ps: projects){
            Project p = new Project();
            for(int i=0; i<ps; i++){
                p.addTeam(new Team(p));
            }
            office.apply(p);
        }
        return office;
    }
}

class Team{
    public  int members;
    public final Project project;

    public Team(int members, Project project){
        this.members = members;
        this.project = project;
    }

    public Team(Project project){
        this(7, project);
    }

    public int leftMembers(){
        return members;
    }

    public void occupy(Box box){
        int mem = box.occupy(this);
        members -= mem;
    }

    public boolean isEmpty(){
        return members == 0;
    }
}

class Project{
    Set<Team> teams = new HashSet();

    public void addTeam(Team team){
        teams.add(team);
    }

}

class Office{

    final int floorSize;
    List<Floor> floors = new ArrayList();
    int curFloor = 0;

    public Office(int floorSize){
        this.floorSize = floorSize;
        checkFloor();
    }

    public void apply(Project project){
        for(Team team: project.teams){
            while(!team.isEmpty()) {
                floors.get(curFloor).applyTeam(team);
                if(!team.isEmpty()){
                    curFloor++;
                    checkFloor();
                }
            }
        }
    }

    private void checkFloor(){
        if(floors.size() <= curFloor){
            floors.add(new Floor(floorSize));
        }
    }
}

class Floor{
    List<Box> boxes = new ArrayList();
    public Floor(int size){
        for(int i=0; i<size; i++){
            boxes.add(new Box());
        }
    }

    public void applyTeam(Team team){
        for(int i=0; i<boxes.size(); i++){
            if(team.isEmpty())
                break;
            team.occupy(boxes.get(i));
        }
    }
}

class Box{
    int places;
    private Project project;

    public Box(int places){
        this.places = places;
    }

    public Box(){
        this(4);
    }

    public int freePlaces(){
        return places;
    }

    public int occupy(Team team){
        if(places == 0 || team.members == 0)
            return 0;
        if(project == null){
            project = team.project;
        }else if(project != team.project)
            return 0;
        int r = Math.min(team.members, places);
        places -= r;
        return r;
    }
}
